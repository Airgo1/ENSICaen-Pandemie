/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 
 
 /**
 * @author Burdet Nicolas <burdet@b35pc23.ecole.ensicaen.fr>
 * @version 0.0.1 / 29-05-2018
 */

/**
 * @file fonction.c
 */

#include <epidemie.h>

void traiter_erreur(char *message)
{
   fprintf(stderr, "%s", message);
   exit(EXIT_FAILURE);
}

/**
* Traitant pour sortir de la boucle infinie et quitter en libérant la mémoire.
*/
void traiter_sigint(int signum)
{
   if (shm_unlink("CHEMIN_MEMOIRE") < 0) {
      traiter_erreur("Erreur lors de l'appel à shm_unlink\n");
   }
   exit(EXIT_SUCCESS);
}

void handle_fatal_error (char *msg) {
   perror(msg);
   exit(EXIT_FAILURE);
}

/**
* Ouverture du sémaphore POSIX de nom "NOM_SEMAPHORE". A faire par le
* processus participant et doit exister.
*/
semaphore_t * ouvrir_semaphore() {
	semaphore_t *sem;
	sem = sem_open(CHEMIN_MEMOIRE, O_RDWR, S_IRUSR|S_IWUSR, 0);
	return sem;
}

/**
* Opération P() - prise du sémaphore : à faire avant tout appel
* à une ressource critique, ici un accès (lecture ou écriture)
* à la mémoire partagée.
*/
void p(semaphore_t * sem) {
	if (sem_wait(sem) < 0) {
		traiter_erreur("Erreur d'appel de l'opération P()\n");
	}
}

/**
* Opération V() - libération du sémaphore : à faire après tout
* appel à une ressource critique, ici un accès (lecture ou
* écriture) à la mémoire partagée.
*/
void v(semaphore_t * sem) {
	if (sem_post(sem) < 0) {
		traiter_erreur("Erreur d'appel de l'opération V()\n");
	}
}

/**
* fermeture du sémaphore.
*/
void fermer_semaphore(semaphore_t * sem) {
	sem_close(sem);
}
