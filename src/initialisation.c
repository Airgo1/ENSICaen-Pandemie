/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 
 
 /**
 * @author Burdet Nicolas <burdet@b35pc23.ecole.ensicaen.fr>
 * @version 0.0.1 / 29-05-2018
 */

/**
 * @file initialisation.c
 */
 
#include <epidemie.h>

void init () {
	pid_t processus = fork();
   
   if (processus < 0) {
      handle_fatal_error("Error using fork().\n");
   }
   if (processus > 0) {
      initserveur();
      
	} else {	
		initsimulation();	
	}
	
	processus = fork();
	
	if (processus > 0) {
      
	} else {
		
		afficher_carte();	
	}
	
	while(processus!=0);
}


