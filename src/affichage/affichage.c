/* -*- c-basic-offset: 3 -*- 
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin 
 * F-14050 Caen Cedex 
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 
 
 /**
 * @author Burdet Nicolas <burdet@b35pc23.ecole.ensicaen.fr>
 * @version 0.0.1 / 29-05-2018
 */

/**
 * @file affichage.c
 */
 
#include <epidemie.h>

void afficher_carte () {
	int i,j;
	
	semaphore_t *ville;
	struct Ville *memoire;/* pointeur sur le segment mémoire "projeté" */

	int descripteur_memoire;
	int taille_memoire;

	taille_memoire = (1 * sizeof(struct Ville));

	signal(SIGINT, traiter_sigint);
	
	/* - 1 - crée le segment de mémoire partagée comme un fichier */
	descripteur_memoire = shm_open(CHEMIN_MEMOIRE, O_RDWR, S_IRUSR|S_IWUSR);

	if (descripteur_memoire < 0) {
		traiter_erreur("Erreur lors de l'appel à shm_open\n");
	}

	/* - 3 - projette le segment de mémoire partagée dans l'espace mémoire du ←-processus */
	memoire = (struct memory_t *) mmap(
		NULL,
		taille_memoire,
		PROT_WRITE,
		MAP_SHARED,
		descripteur_memoire,
		0);
		
	while (1) {
		if (memoire->affiche ==1) {
			for (i=0;i<7;i++) {
				for (j=0;j<7;j++) {
					printf("%d ", memoire->matrice[i][j].bat.type);
				}
				printf("\n");
			}
			
			printf("\n \n");
			printf("Contamination : \n");
			
			for (i=0;i<7;i++) {
				for (j=0;j<7;j++) {
					printf("%2lf ", memoire->matrice[i][j].bat.contamination);
				}
				printf("\n");
			}	
		}	
	}
	
	if (memoire == MAP_FAILED) {
		traiter_erreur("Erreur lors de l'appel à mmap\n");;
	}
	
	for (i=0;i<7;i++) {
		for (j=0;j<7;j++) {
			printf("%d ", memoire->matrice[i][j].bat.type);
		}
		printf("\n");
	}
	
	/*
	* - 5 - ferme le segment de mémoire partagée - Jamais atteint d'où la gestion
	* par interception du signal SIGINT (<CTRL-C>).
	*/
	if (shm_unlink(CHEMIN_MEMOIRE) != 0) {
		traiter_erreur("Erreur lors de l'appel à shm_unlink\n");
	}
	exit(EXIT_SUCCESS);

	
}
