#!/bin/sh
#
# GREYC IMAGE
# 6 Boulevard Mar�chal Juin
# F-14050 Caen Cedex France
#
# This file is owned by ENSICAEN students.
# No portion of this document may be reproduced, copied
# or revised without written permission of the authors.

#
# (C)Burdet Nicolas - D-05-2018
#

bin/exe: obj/citoyen.o obj/affichage.o obj/simulation.o obj/serveur.o obj/initialisation.o obj/fonction.o obj/main.o
	gcc -o bin/exe obj/*.o  -lrt -lm -pthread 
	
obj/affichage.o : src/affichage/affichage.c
	gcc -o obj/affichage.o -c src/affichage/affichage.c -Wall -g -pedantic -I./include/
	
obj/citoyen.o : src/simulation/citoyen.c
	gcc -o obj/citoyen.o -c src/simulation/citoyen.c -Wall -g -pedantic -I./include/

obj/simulation.o : src/simulation/simulation.c
	gcc -o obj/simulation.o -c src/simulation/simulation.c -Wall -g -pedantic -I./include/

obj/serveur.o : src/serveur/serveur.c
	gcc -o obj/serveur.o -c src/serveur/serveur.c -g -pedantic -I./include
	
obj/initialisation.o: src/initialisation.c
	gcc -o obj/initialisation.o -c src/initialisation.c -Wall -g  -pedantic -I./include
	
obj/fonction.o : src/fonction.c
	gcc -o obj/fonction.o -c src/fonction.c -Wall -g -pedantic -I./include/

obj/main.o: src/main.c 
	gcc -o obj/main.o -c src/main.c -Wall -g -pedantic -I./include
	
Clean : 
		rm obj/*.o 
